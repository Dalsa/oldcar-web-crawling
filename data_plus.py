from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import time

driver = webdriver.Firefox()

#크롤링 여려 페이지 크롤링 시도하기
car_html_source = ""
for page_number in range(1, 251):
    URL = "http://auto.danawa.com/usedcar/?Work=list&Tab=list&Page=" + str(page_number)
    driver.get(URL)
    html = driver.page_source
    soup = BeautifulSoup(html,'html.parser')
    car_html_source = car_html_source + str(soup)
    time.sleep(1)
    print("{0} 번째 진행중입니다.".format(page_number))

soup = BeautifulSoup(car_html_source,'html.parser')

with open("car.txt", "w", encoding="utf-8") as f:
    f.write(str(soup))

name_list = soup.select(
    "html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.info div.model div.middle a.name"
)

car_labs = soup.select(
    "html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.info div.model div.middle div.spec"
)

year = soup.select(
    'html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.year div.middle'
)

load_km = soup.select(
    "html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.km div.middle"
)

money = soup.select(
    'html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.price div.middle span.num'
)

date = soup.select(
    'html body div#autodanawa_wrap section div#autoshop_content div#autodanawa_gridC div.gridMain.noline div.usedcar_container div#result_area.result_area div.result_contents div#usedcarList.result_list ul li.row div.col.store div.middle span.date'
)

car_data = []
for item in zip(name_list, car_labs, year, load_km, money, date):
    car_data.append(
        {
            'name' : item[0].text,
            'car_labs' : item[1].text,
            'year' : item[2].text,
            'load_km' : item[3].text,
            'money' : item[4].text,
            'date' : item[5].text
        }
    )

car_df = pd.DataFrame(car_data)
print(car_df)
car_df.to_csv('car.csv', encoding='cp949', index_label=None)

driver.close()